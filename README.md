An attempt to get the code for my diss to compile again.

Note that the actual code of my diss in is a private dune-pm module.  That
module used to be an internal module of the Heidelberg group and contains some
other PDE solvers from other people, which I can't just make public.  And I
currently don't have the time to disentangle those parts.  Therefore the
dune-pm module as a whole has to stay private.
